#!/usr/bin/env python3
# original file: "pipeline-tools/CWL/biomed/biomed_upload.py"
# Retrieved from pipeline-tools repository

import argparse
import logging
import sys
from time import strftime, gmtime

import os
from unittest.mock import patch

from biomed.auth import TokenAuth
from biomed.client import BiomedApiClient
from biomed.exceptions import BiomedResponseError
from biomed.models.data import LocalData
from biomed.schemas.data import LocalDataSchema

def finalize_with_error():
    logging.error("Could not finish upload", flush=True)
    sys.exit("Failed execution")


def create_data_object(file_to_upload, output_path):
    d = dict(local_path=file_to_upload, path=output_path)
    local_data = LocalData.from_schema(schema=LocalDataSchema(), data=d)
    return local_data


def upload_file(api, file_to_upload, output_path, id_project):
    local_data = create_data_object(file_to_upload, output_path)
    uploaded_file = api.project(id_project).data.upload(local_data)
    print(f'[{strftime("%Y-%m-%d %H:%M:%S", gmtime())}] Uploaded: {uploaded_file.path}')


def main():
    # Input parameters
    parser = argparse.ArgumentParser(description='Download file')
    # Fixed in the task template
    parser.add_argument('--input-file', required=True, type=str, help="File to upload")
    parser.add_argument('--output-path', required=True, type=str, help="Output file path")

    # Fixed in the docker environment
    parser.add_argument('--token', required=True, type=str, help="Token")
    parser.add_argument('--refresh-token', required=True, type=str, help="Refresh Token")
    parser.add_argument('--id-project', required=True, type=int, help="Project ID")

    args = parser.parse_args()

    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s [%(levelname)s] -- %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        stream=sys.stdout
        #filename="/tmp/biomed_upload.log"
    )

    auth = TokenAuth(token=args.token, refresh_token=args.refresh_token)
    api = BiomedApiClient(auth)

    try:
        upload_file(api, args.input_file, args.output_path, args.id_project)
        sys.exit(0)
    except BiomedResponseError as e:
        print(e)
        finalize_with_error()


if __name__ == "__main__":
    with patch.dict(os.environ, {'BIOMED_ENV': 'test-dev'}): #It must be test-dev because with dev it needs to be connected to the VPN
        main()
