#!/usr/bin/env python3

import argparse
import logging
import sys
import json
import os
from unittest.mock import patch
from marshmallow import ValidationError

from biomed.auth import TokenAuth
from biomed.client import BiomedApiClient
from biomed.exceptions import BiomedResponseError
from biomed.schemas.record import RecordSchema

def finalize_with_error():
    logging.error("Could not finish upload", flush=True)
    sys.exit("Failed execution")

def main():
    # Input parameters
    parser = argparse.ArgumentParser(description='Download file')

    # Arguments neccesary to retrieve the biomed client API object
    parser.add_argument('--token', required=True, type=str, help="Token")
    parser.add_argument('--refresh-token', required=True, type=str, help="Refresh Token")
    parser.add_argument('--id-project', required=True, type=int, help="Project ID")
    args = parser.parse_args()

    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s [%(levelname)s] -- %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
        stream=sys.stdout
        #filename="/tmp/biomed_upload.log"
    )

    auth = TokenAuth(token=args.token, refresh_token=args.refresh_token)
    api = BiomedApiClient(auth)

    try:
        record = json.load(sys.stdin)
        RecordSchema().load(record)
        response = api.record.create(data=record)
        logging.info(f"Record successully uploaded with {response.id} unique ID.")
        sys.exit(0)
    except BiomedResponseError as e:
        print(e)
        finalize_with_error()
    except ValidationError as e:
        print(e.messages)
        finalize_with_error()


if __name__ == "__main__":
    with patch.dict(os.environ, {'BIOMED_ENV': 'dev'}):
        main()
