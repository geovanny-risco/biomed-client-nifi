from org.apache.commons.io import IOUtils
from java.nio.charset import StandardCharsets
from org.apache.nifi.processor.io import StreamCallback
from java.lang import Throwable

import json

STEP = 500

class PyStreamCallback(StreamCallback):
    def __init__(self):
            pass
    def process(self, inputStream, outputStream):
        flowfile_content = IOUtils.toString(inputStream, StandardCharsets.UTF_8)
        hl7_json = json.loads(flowfile_content)
        transformJSON = TransformJSON()
        transformJSON(hl7_json)
        outputStream.write(bytearray(json.dumps(hl7_json, indent=4).encode('utf-8')))

class TransformJSON():

    def __call__(self, hl7_json):
        obr_section = hl7_json.pop("OBR")
        orc_section = hl7_json.pop("ORC") # 1 OBR -> 1 ORC
        obx_section = hl7_json.pop("OBX") # 1 OBR -> [1..N] OBX (maximum of 500)
        nte_section = hl7_json.pop("NTE", {}) # 1 OBR -> [0..N] NTE (maximum of 500)
        ft1_section = hl7_json.pop("FT1", {}) # 1 OBR -> [0..N] FT1 (maximum of 500)
    
        hl7_json["ORDER_OBSERVATION"] = []
        for observation_index in range(1, len(obr_section)+1):
            observation = {}

            # Get all the OBX, NTE, FT1 indexes that corresponds to this observation set ID. This must be done to all those segments that have N possible relations. 
            observation_setID = obr_section[str(observation_index)].get("SetIDOBR")
            obx_indexes = self.searchObservationSetID(int(observation_setID), obx_section, "SetIDOBX")
            if not obx_indexes: #NOTE: There must be at least one OBX for each OBR
                raise Exception("Each Observation must have at least one OBX")
            nte_indexes = self.searchObservationSetID(int(observation_setID), nte_section, "SetIDNTE")
            ft1_indexes = self.searchObservationSetID(int(observation_setID), ft1_section, "SetIDFT1")
            
            # Reordered all items
            observation["ORC"] = orc_section.pop(str(observation_index))
            observation["OBR"] = obr_section.pop(str(observation_index))
            observation["OBX"] = [obx_section.pop(obx_index) for obx_index in obx_indexes] #TODO: order OBXs by setID
            observation["NTE"] = [obx_section.pop(nte_index) for nte_index in nte_indexes]
            observation["FT1"] = [obx_section.pop(ft1_index) for ft1_index in ft1_indexes]
            hl7_json["ORDER_OBSERVATION"].append(observation)

        if obr_section or orc_section or obx_section or nte_section or ft1_section: # They all must be empty
            raise Exception("There some elements left to push into ORDER_OBSERVATIONS")

    @staticmethod
    def searchObservationSetID(observation_setID, section, setIDname):
        found_indexes = []
        for index, data in section.items():
            if int(data.get(setIDname)) in range(observation_setID, observation_setID+STEP):
                found_indexes.append(index)
        return found_indexes


flowFile = session.get()
if(flowFile != None):
    try:
        flowFile = session.write(flowFile, PyStreamCallback())
        session.transfer(flowFile, REL_SUCCESS)
    except Throwable, e:
        log.error('Something went wrong', e)
        session.transfer(flowFile, REL_FAILURE)
